
prism.registerWidget("helloworld", {
	name: "helloworld",
	title: "Hello World",
	hideNoResults: true,
	iconSmall: "/plugins/HelloWorldWidget/icon-24.png",
	styleEditorTemplate: "/plugins/HelloWorldWidget/styler.html",
	style: {
		textAlign:"left"
	},
	data: {
		selection: [],
		defaultQueryResult: {},
		panels: [
			{
				name: 'category',
				type: "visible",
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: 'value',
				type: 'visible',
				metadata: {
					types: ['measures'],
					maxitems: 1
				}
			},
			{
				name: 'filters',
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],

		// 	Build a JAQL query for this widget
		buildQuery: function (widget) {

			// building jaql query object from widget metadata 
			var query = {
				datasource: widget.datasource,
				format: "json",
				isMaskedResult: true,
				offset: 0,
				count: 10000,
				metadata: []
			};

			//	Add categories to the query
			widget.metadata.panel("category").items.forEach(function(item){
				query.metadata.push(item);
			})

			//	Add values to the query
			widget.metadata.panel("value").items.forEach(function(item){
				query.metadata.push(item);
			})

			//	Add widget filters to the query
			widget.metadata.panel('filters').items.forEach(function (item) {
				
				//	Create a copy of the item
				item = $$.object.clone(item, true);

				//	Specify that this is a filter
				item.panel = "scope";

				//	Add to query
				query.metadata.push(item);
			});

			//	Dashboard filters are added automatically to the query
			return query;
		},

		//	Transform the data returned from the query
		processResult: function (widget, queryResult) {

			//	Create an object to hold the new query results
			var newResults = {
				data: [],
				stats: {
					rowCount: null
				}
			};

			//	Get the raw data
			newResults.data = queryResult.$$rows;

			//	Figure out how many results were returned
			newResults.stats.rowCount = newResults.data.length;

			//	Return the results
			return newResults;
		}
	},

	//	Take the transformed data, and render to the widget
	render: function (widget, args) {

		//	Write the data to the console, just for viewing
		console.log("Widget Data:");
		console.log(widget.queryResult);

		// 	Get the DOM element for this widget
		var element = $(args.element);
		element.empty();

		//	Create the Hello World text as HTML (including style)
		var myText = '<div style="text-align:' + widget.style.textAlign + ';">'
						+ '<p>Hello World!</p>'
					+ '</div>';

		//	Add to the DOM
		element.append(myText);
	},
	destroy: function (widget, args) { }
});