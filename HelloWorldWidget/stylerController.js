
mod.controller('stylerController', ['$scope', function ($scope) {

        //  Bind the widget's style property, to this controller's scope
        $scope.$watch('widget', function (val) {
            $scope.model = $$get($scope, 'widget.style');
        });

        //  Function to allow the design panel, to set style properties of the widget
        $scope.setStyle = function (propertyName, propertyValue) {
        	
            // Set the style property
            $scope.model[propertyName] = propertyValue;
        	
            //  Wait for the property to be set, then redraw widget
            _.defer(function () {
        		$scope.$root.widget.redraw();
        	});
        };
    }
]);